<?php

class Users_Model extends CI_Model {
    
    public $id;
    public $first_name;
    public $last_name;
    
    public function getList() {
        $query = $this->db->get('users');
        return $query->result();
    }
    
    public static function remove($id) {
        if (!$id) {
            return false;
        }
        
        $obj = new self;
        $obj->db->delete('users', array('id'=>$id));
    }
    
    public static function make($id, $firstName, $lastName) {
        if (!$id || !$firstName || !$lastName) {
            return array(
                'error' => 1,
                'msg' => 'Введите все данные'
            );
        }
        
        $obj = new self;
        $obj->id = $id;
        $obj->first_name = $firstName;
        $obj->last_name = $lastName;
        
        $res = $obj->db->insert('users', $obj);
        
        return array('error'=>0);
    }
    
}