<?php

class Messages_Model extends CI_Model {
    
    public $id;
    public $user_id;
    public $text;
    
    public function getList() {
        $query = $this->db->get('messages');
        return $query->result();
    }
    
    public static function make($id, $user_id, $text) {
        if (!$id || !$user_id || !$text) {
            return array(
                'error' => 1,
                'msg' => 'Введите все данные'
            );
        }
        
        $obj = new self;
        $obj->id = $id;
        $obj->user_id = $user_id;
        $obj->text = $text;
        
        $res = $obj->db->insert('messages', $obj);
        
        return array('error'=>0);
    }
}