<html>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>x</th>
            </tr>
            <?php foreach ($list as $user): ?>
            <tr>
                <td><?php echo $user->id?></td>
                <td><?php echo $user->first_name?></td>
                <td><?php echo $user->last_name?></td>
                <td>
                    <a href="../codeigniter/index.php/welcome/remove?id=<?php echo $user->id?>">
                    Удалить
                    </a>
                </td>                    
            </tr>
            <?php endforeach ?>
        </table>
        
        <h4>Добавление пользователя</h4>
        <?php if ($error && $error['error']): ?>
        <p>Ошибка: <?php echo $error['msg']?></p>
        <?php endif ?>
        
        <form action="../codeigniter/index.php" method="post">
            <p>ID:</p>
            <input type="text" name="id">
            <p>Имя:</p>
            <input type="text" name="first_name">
            <p>Фамилия:</p>
            <input type="text" name="last_name">
            <input type="submit" value="Добавить">
        </form>
    </body>
</html>