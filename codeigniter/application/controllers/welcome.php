<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
    
        public function __construct() {
            parent::__construct();
            
            $this->load->model('Messages_Model');
        }

        public function index() {
            if ($_POST) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $text = $_POST['text'];
                
                $error = Messages_Model::make($id, $user_id, $text);
            }
            
            $obj = new Messages_Model();
            $list = $obj->getList();
            
            $this->load->view('messages_users', array(
                'list' => $list,
                'error' => isset($error) ? $error : null
            ));
        }
        
      
}

//class Welcome extends MY_Controller {
//    
//        public function __construct() {
//            parent::__construct();
//            
//            $this->load->model('Users_Model');
//        }
//
//        public function index() {
//            if ($_POST) {
//                $id = $_POST['id'];
//                $fname = $_POST['first_name'];
//                $lname = $_POST['last_name'];
//                
//                $error = Users_Model::make($id, $fname, $lname);
//            }
//            
//            $obj = new Users_Model();
//            $list = $obj->getList();
//            
//            $this->load->view('users', array(
//                'list' => $list,
//                'error' => isset($error) ? $error : null
//            ));
//        }
//        
//        public function remove() {
//            $id = isset($_GET['id']) ? $_GET['id'] : null;
//            Users_Model::remove($id);
//            
//            header('location: /codeigniter/index.php');
//        }
//}