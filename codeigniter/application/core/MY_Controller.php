<?php

/**
 * Базовый контроллер от которого будем наследовать наши контроллеры
 */
abstract class MY_Controller extends CI_Controller {
        
        /**
         * В конструкторе задаем кодировку для страниц
         */
        public function __construct() {
            parent::__construct();
            header('Content-Type: text/html; charset=utf-8');
        }    
        
}