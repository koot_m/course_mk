<?php
/* @var $this TestController */
$this->breadcrumbs=array(
    'Test',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>
<?php foreach ($list as $obj): ?>
    <p><?php echo $obj->id.' '.$obj->text?></p>
<?php endforeach; ?>
<?php if ($page > 0): ?>
    <a href="/index.php?r=test/index&page=<?php echo $page-1?>">
        <<
    </a>
<?php endif ?>
    
<?php if (count($list) >= $countInPage): ?>
    <a href="/index.php?r=test/index&page=<?php echo $page+1?>">
        >>
    </a>
<?php endif ?>