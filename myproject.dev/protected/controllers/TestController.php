<?php
class TestController extends Controller
{
    const COUNT_IN_PAGE = 5;
    
   public function actionIndex()
    {
            $req = Yii::app()->request;
            $page = $req->getParam('page', 0);
            
            $offset = $page * self::COUNT_IN_PAGE;
            $list = Test::getList(array(), self::COUNT_IN_PAGE, $offset);
            
            $this->render('index', array(
                'list' => $list,
                'page' => $page,
                'countInPage' => self::COUNT_IN_PAGE
            ));
    }
    
    public function actionSession() {
            /** @var CHttpSession */
            $session = Yii::app()->session;
            
            foreach ($session as $key => $value) {
                echo $key .' = '. $value .'<br>';
            }
            
            if (!isset($session['inc'])) {
                $session['inc'] = 0;
            } else {
                $session['inc'] += 1;
            }            
        }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}