<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $login
 * @property string $password
 */
class Users extends BaseUsers
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * Метод поиска по login
         * @param string $login
         * @return User|false
         */
        public static function getForLogin($login) {
            return self::model()->findByAttributes(array('login'=>$login));
        }
}
