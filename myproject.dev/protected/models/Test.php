<?php

/**
 * This is the model class for table "test".
 *
 * The followings are the available columns in table 'test':
 * @property integer $id
 * @property integer $time
 * @property integer $user_id
 * @property string $text
 */
class Test extends BaseTest
{
	public static function getList($params = array(), $limit, $offset = 0) {
            $cr = new CDbCriteria();
            
            // заполняем условия WHERE
            if ($params) {
                $cr->addColumnCondition($params);
            }
            
            // добавляем сортировку после ORDER BY
            $cr->order = 'id desc';
            
            // заполняем данные для LIMIT
            $cr->offset = $offset;
            $cr->limit = $limit;
            
            
            return self::model()->findAll($cr);
        }
}
