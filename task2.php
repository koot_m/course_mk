<?php

function init($name, $last_name, $year) {
	return (object)array(
    'name'        => $name,
    'last_name'   => $last_name,
    'year'        => $year
    );
}

$obj = init('Konstantin','Mamontov',1975);
var_dump ($obj);

function set ($obj1, $name, $last_name, $year){
    $obj1->name = $name;
    $obj1->last_name = $last_name;
    $obj1->year = $year;
}

set($obj, 'Petr', 'Ivanov', 1980);
var_dump($obj);